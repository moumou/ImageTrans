# ImageTrans

#### 项目介绍
图片(旋转/缩放/翻转/拖动)变换效果(ccs3/滤镜/canvas)  
兼容：ie6/7/8, firefox 3.6.8, opera 10.6, safari 5.0.1, chrome 5.0

演示路径  https://ljt0515.gitee.io/imagetrans/ImageTrans.htm
